﻿using ATS.Repositories;
using ATS.models;
using Moq;
using ATS.Services;
using Microsoft.AspNetCore.Http;

namespace ATS.test;

[TestFixture]
public class CandidatoServiceTest
{
    [Test]
    public void GetAll()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();
        var service = new CandidatoService(mockRepositorio.Object);
        var expectedList = new Mock<List<Candidato>>();
        mockRepositorio.Setup(r => r.GetAll()).Returns(expectedList.Object);
        var result = service.GetAll();
        Assert.That(result, Is.EqualTo(expectedList.Object));
    }
    [Test]
    public void GetAllFail()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();
        var service = new CandidatoService(mockRepositorio.Object);
        var expectedList = new Mock<List<Candidato>>();
        var newList = new List<Candidato>() { new Candidato
        {
            Cpf = "039284123",
        } };
        mockRepositorio.Setup(r => r.GetAll()).Returns(expectedList.Object);
        var result = service.GetAll();
        Assert.That(result, Is.Not.EqualTo(newList));
    }
    [Test]
    public void ObterPorId()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();
        var service = new CandidatoService(mockRepositorio.Object);
        var expectedCandidato = new Mock<Candidato>();
        mockRepositorio.Setup(r => r.ObterPorId(It.IsAny<Guid>())).Returns(expectedCandidato.Object);
        var result = service.ObterPorId(Guid.NewGuid());
        Assert.That(result, Is.EqualTo(expectedCandidato.Object));
    }
    [Test]
    public void AddCandidato()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();

        var service = new CandidatoService(mockRepositorio.Object);
        //Setup mock file using a memory stream
        var content = "Hello World from a Fake File";
        var fileName = "test.pdf";
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(content);
        writer.Flush();
        stream.Position = 0;
        IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);

        var obj = new CandidatoForm
        {
            Nome = "teixeira",
            Cpf = "07323676523",
            CV = file
        };
        service.Inserir(obj);
        mockRepositorio.Verify(r => r.Inserir(It.IsAny<Candidato>()), Times.Once);
    }
    [Test]
    public void UpdateCandidato()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();

        var service = new CandidatoService(mockRepositorio.Object);
        //Setup mock file using a memory stream
        var content = "Hello World from a Fake File";
        var fileName = "test.pdf";
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(content);
        writer.Flush();
        stream.Position = 0;
        IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);

        var obj = new CandidatoForm
        {
            Id = Guid.NewGuid(),
            Nome = "teixeira",
            Cpf = "07323676523",
            CV = file
        };
        service.Atualizar(obj);
        mockRepositorio.Verify(r => r.Atualizar(It.IsAny<Candidato>()), Times.Once);
    }
    [Test]
    public void DeleteCandidato()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();

        var service = new CandidatoService(mockRepositorio.Object);
        Guid id = Guid.NewGuid();
        service.Remover(id);
        mockRepositorio.Verify(r => r.Remover(id), Times.Once);
    }
    [Test]
    public void DeleteCandidatoFail()
    {
        var mockRepositorio = new Mock<IRepository<Candidato>>();

        var service = new CandidatoService(mockRepositorio.Object);
        Guid id = Guid.NewGuid();
        service.Remover(id);
        mockRepositorio.Verify(r => r.Remover(Guid.NewGuid()), Times.Never);
    }
}
