﻿using System;
namespace ATS.models
{
	public class CandidatoForm
	{
        public Guid? Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public IFormFile CV { get; set; }
    }
}

