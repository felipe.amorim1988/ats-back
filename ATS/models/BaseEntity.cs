﻿using System;
namespace ATS.models
{
	public abstract class BaseEntity
	{
		public Guid? Id { get; set; }
	}
}

