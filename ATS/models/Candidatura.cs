﻿using System;
namespace ATS.models
{
	public class Candidatura
	{
		public Guid Id { get; set; }
		public Candidato Candidato { get;set; }
		public Vaga Vaga { get; set; }
	}
}

