﻿using System;
namespace ATS.models
{
	public class Candidato : BaseEntity
    {
		public string Nome { get; set; }
		public string Cpf { get; set; }
        public byte[] CV { get; set; }

    }
}

