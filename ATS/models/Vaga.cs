﻿using System;
namespace ATS.models
{
	public class Vaga : BaseEntity
    {
		public string Titulo { get; set; }
		public string Descricao { get; set; }
	}
}

