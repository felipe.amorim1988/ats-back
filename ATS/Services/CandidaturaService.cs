﻿using System;
using ATS.models;
using ATS.Repositories;

namespace ATS.Services
{
	public interface ICandidaturaService
	{
		void Inserir(Guid CandidatoId, Guid VagaId);
        List<Candidato> GetCandidatos(Guid VagaId);
		void DeleteCascade(Guid VagaId);
    }

    public class CandidaturaService : ICandidaturaService
    {
        private readonly IRepository<Candidato> _CandidatoRepository;
		private readonly IRepository<Vaga> _VagaRepository;
		private readonly ICandidaturaRepository _CandidaturaRepository;

        public CandidaturaService(
			IRepository<Candidato> CandidatoRepository,
			IRepository<Vaga> VagaRepository,
            ICandidaturaRepository CandidaturaRepository)
		{
            _CandidatoRepository = CandidatoRepository;
			_VagaRepository = VagaRepository;
			_CandidaturaRepository = CandidaturaRepository;
        }

		public void Inserir(Guid CandidatoId, Guid VagaId)
		{
			var candidato = _CandidatoRepository.ObterPorId(CandidatoId);
			var vaga = _VagaRepository.ObterPorId(VagaId);
			_CandidaturaRepository.Inserir(candidato, vaga);

        }
        public List<Candidato> GetCandidatos(Guid VagaId)
        {
			return _CandidaturaRepository.GetCandidatos(VagaId);

        }
		public void DeleteCascade(Guid VagaId)
		{
            _CandidaturaRepository.DeleteCascade(VagaId);
        }
    }
}

