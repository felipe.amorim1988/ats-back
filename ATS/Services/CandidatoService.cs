﻿using System;
using ATS.models;
using ATS.Repositories;

namespace ATS.Services
{
    public interface ICandidatoService
    {
        List<Candidato> GetAll();
        void Inserir(CandidatoForm candidato);
        Candidato ObterPorId(Guid id);
        void Atualizar(CandidatoForm Candidato);
        void Remover(Guid id);
    }
    public class CandidatoService : ICandidatoService
    {
        private readonly IRepository<Candidato> _CandidatoRepository;

        public CandidatoService(IRepository<Candidato> CandidatoRepository)
        {
            _CandidatoRepository = CandidatoRepository;
        }

        public List<Candidato> GetAll()
        {
            return _CandidatoRepository.GetAll();
        }

        public async void Inserir(CandidatoForm candidato)
        {

            var obj = new Candidato
            {
                Id = Guid.NewGuid(),
                Nome = candidato.Nome,
                Cpf = candidato.Cpf,
                CV = await ReadFully(candidato.CV.OpenReadStream())
            };
            _CandidatoRepository.Inserir(obj);
        }

        public Candidato ObterPorId(Guid id)
        {
            return _CandidatoRepository.ObterPorId(id);
        }

        public async void Atualizar(CandidatoForm candidato)
        {
            var obj = new Candidato
            {
                Id = candidato.Id,
                Nome = candidato.Nome,
                Cpf = candidato.Cpf,
                CV = await ReadFully(candidato.CV.OpenReadStream())
            };
            _CandidatoRepository.Atualizar(obj);
        }
        public void Remover(Guid id)
        {
            _CandidatoRepository.Remover(id);
        }

        private async Task<byte[]> ReadFully(Stream input)
        {
            using (var ms = new MemoryStream())
            {
                await input.CopyToAsync(ms);
                return ms.ToArray();
            }
        }

    }

}

