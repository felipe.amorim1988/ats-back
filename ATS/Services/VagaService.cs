﻿using System;
using ATS.models;
using ATS.Repositories;

namespace ATS.Services
{
    public interface IVagaService
    {
        List<Vaga> GetAll();
        Vaga ObterPorId(Guid id);
        void Inserir(Vaga Vaga);
        void Atualizar(Vaga Vaga);
        void Remover(Guid id);
    }
    public class VagaService : IVagaService
    {
        private readonly IRepository<Vaga> _VagaRepository;
        private readonly ICandidaturaService _candidaturaService;

        public VagaService(IRepository<Vaga> VagaRepository, ICandidaturaService candidaturaService)
        {
            _VagaRepository = VagaRepository;
            _candidaturaService = candidaturaService;
        }

        public List<Vaga> GetAll()
        {
            return _VagaRepository.GetAll();
        }

        public Vaga ObterPorId(Guid id)
        {
            return _VagaRepository.ObterPorId(id);
        }

        public async void Inserir(Vaga Vaga)
        {

            var obj = new Vaga
            {
                Id = Guid.NewGuid(),
                Titulo = Vaga.Titulo,
                Descricao = Vaga.Descricao
            };
            _VagaRepository.Inserir(obj);
        }

        public void Atualizar(Vaga Vaga)
        {
            _VagaRepository.Atualizar(Vaga);
        }
        public void Remover(Guid id)
        {
            _VagaRepository.Remover(id);
            _candidaturaService.DeleteCascade(id);
        }

    }

}

