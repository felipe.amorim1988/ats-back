﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ATS.models;
using ATS.Services;
using Microsoft.AspNetCore.Mvc;

namespace ATS.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VagaController : ControllerBase
    {
        private readonly IVagaService _service;
        public VagaController(IVagaService service)
        {
            _service = service;
        }
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Vaga obj)
        {
            _service.Inserir(obj);
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(_service.GetAll());
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(Guid Id)
        {
            return Ok(_service.ObterPorId(Id));
        }
        [HttpPatch]
        public async Task<IActionResult> Update([FromBody] Vaga obj)
        {
            _service.Atualizar(obj);
            return Ok();
        }
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Remove(Guid Id)
        {
            _service.Remover(Id);
            return Ok();
        }
    }
}

