﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ATS.models;
using ATS.Services;
using Microsoft.AspNetCore.Mvc;

namespace ATS.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CandidatoController : ControllerBase
    {
        private readonly ICandidatoService _service;
        public CandidatoController(ICandidatoService service)
        {
            _service = service;
        }
        [HttpPost]
        public async Task<IActionResult> Add([FromForm] CandidatoForm obj)
        {
            _service.Inserir(obj);
            return Ok();
        }
        [HttpPatch]
        public async Task<IActionResult> Update([FromForm] CandidatoForm obj)
        {
            _service.Atualizar(obj);
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(_service.GetAll());
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(Guid Id)
        {
            return Ok(_service.ObterPorId(Id));
        }
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Remove(Guid Id)
        {
            _service.Remover(Id);
            return Ok();
        }
    }
}

