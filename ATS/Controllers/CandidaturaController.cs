﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ATS.models;
using ATS.Services;
using Microsoft.AspNetCore.Mvc;

namespace ATS.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CandidaturaController : ControllerBase
    {
        private readonly ICandidaturaService _service;
        public CandidaturaController(ICandidaturaService service)
        {
            _service = service;
        }
        [HttpPost("{CandidatoId}/{VagaId}")]
        public async Task<IActionResult> Add(Guid CandidatoId, Guid VagaId)
        {
            _service.Inserir(CandidatoId,VagaId);
            return Ok();
        }
        [HttpGet("{VagaId}")]
        public async Task<IActionResult> Get(Guid VagaId)
        {
            return Ok(_service.GetCandidatos(VagaId));
        }
    }
}

