﻿using System;
using ATS.models;
using MongoDB.Driver;

namespace ATS.Repositories
{
    public class VagaRepository : Repository<Vaga>
    {
        public VagaRepository(IMongoDatabase database): base(database, "Vagas")
        {
        }
    }
}

