﻿using System;
using ATS.models;

namespace ATS.Repositories
{
	public interface IRepository<T> where T : BaseEntity
    {
        List<T> GetAll();
        T ObterPorId(Guid id);
        void Inserir(T item);
        void Atualizar(T item);
        void Remover(Guid id);
    }
}

