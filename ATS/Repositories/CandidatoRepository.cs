﻿using System;
using ATS.models;
using MongoDB.Driver;

namespace ATS.Repositories
{

    public class CandidatoRepository : Repository<Candidato>
    {
        public CandidatoRepository(IMongoDatabase database) : base(database, "Candidatos")
        {

        }
    }
}

