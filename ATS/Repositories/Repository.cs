﻿using System;
using ATS.models;
using MongoDB.Driver;

namespace ATS.Repositories
{
	public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public Repository(IMongoDatabase database, string collectionName)
		{
            _collection = database.GetCollection<T>(collectionName);
        }
        public List<T> GetAll()
        {
            return _collection.Find(p => true).ToList();
        }
        public T ObterPorId(Guid id)
        {
            return _collection.Find(p => p.Id == id).FirstOrDefault();
        }

        public void Inserir(T item)
        {
            _collection.InsertOne(item);
        }

        public void Atualizar(T item)
        {
            _collection.ReplaceOne(p => p.Id == item.Id, item);
        }

        public void Remover(Guid id)
        {
            _collection.DeleteOne(p => p.Id == id);
        }
    }
}

