﻿using System;
using ATS.models;
using MongoDB.Driver;

namespace ATS.Repositories
{
    public interface ICandidaturaRepository
    {

        void Inserir(Candidato Candidato, Vaga vaga);
        List<Candidato> GetCandidatos(Guid VagaId);
        void DeleteCascade(Guid VagaId);
    }
    public class CandidaturaRepository : ICandidaturaRepository
    {
        private readonly IMongoCollection<Candidatura> _candidatura;

        public CandidaturaRepository(IMongoDatabase database)
		{
            _candidatura = database.GetCollection<Candidatura>("Candidaturas");
        }
        public void Inserir(Candidato candidato, Vaga vaga)
        {
            var obj = new Candidatura
            {
                Id = Guid.NewGuid(),
                Candidato= candidato,
                Vaga = vaga
            };
            _candidatura.InsertOne(obj);
        }
        public List<Candidato> GetCandidatos(Guid VagaId)
        {
            return _candidatura.Find(x => x.Vaga.Id == VagaId).ToList().Select(x => x.Candidato).ToList();

        }
        public void DeleteCascade(Guid VagaId)
        {
            _candidatura.DeleteMany(x => x.Vaga.Id == VagaId);
        }
    }
}

