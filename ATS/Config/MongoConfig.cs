﻿using System;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace ATS.Config
{
	public static class MongoConfig
	{
        public static IServiceCollection MongoContextConfig(this IServiceCollection services, IConfiguration configuration)
        {
            try
            {
                var connectionString = configuration.GetConnectionString("MongoDBConnection");
                services.AddSingleton<IMongoClient>(new MongoClient(connectionString));
                services.AddScoped<IMongoDatabase>(provider =>
                {
                    var client = provider.GetRequiredService<IMongoClient>();
                    return client.GetDatabase("ats");
                });
                return services;
            }
            catch
            {
                throw;
            }
        }
    }
}

