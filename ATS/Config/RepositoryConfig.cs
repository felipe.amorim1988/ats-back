﻿using System;
using ATS.models;
using ATS.Repositories;
using MongoDB.Driver;

namespace ATS.Config
{
	public static class RepositoryConfig
	{
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            try
            {
                services.AddScoped(typeof(IRepository<Candidato>), typeof(CandidatoRepository));
                services.AddScoped(typeof(IRepository<Vaga>), typeof(VagaRepository));
                services.AddScoped<ICandidaturaRepository, CandidaturaRepository>();

                return services;
            }
            catch
            {
                throw;
            }
        }
    }
}

