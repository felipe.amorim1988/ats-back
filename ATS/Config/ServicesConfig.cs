﻿using System;
using ATS.Repositories;
using ATS.Services;

namespace ATS.Config
{
	public static class ServicesConfig
	{
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            try
            {
                services.AddScoped<ICandidatoService, CandidatoService>();
                services.AddScoped<ICandidaturaService, CandidaturaService>();
                services.AddScoped<IVagaService, VagaService>();
                return services;
            }
            catch
            {
                throw;
            }
        }
    }
}

